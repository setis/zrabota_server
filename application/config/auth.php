<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(

	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => 45871851247,
	'lifetime'     => 1209600,
	'session_type' => 'database',
	'session_key'  => 'auth34hd73',

);
?>