<?php

defined('SYSPATH') OR die('No direct script access.');

return array(
    'user' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '10M'
    ),
    'logo' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads/logo',
        'allowed' => array("image/*"),
        'file_is_image' => true,
        'upload_max_filesize' => '10M'
    ),
    'avatar' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'image_convert' => 'jpg',
        'file_overwrite' => true,
        'allowed' => array("image/*"),
        'file_is_image' => true,
        'dir' => '/var/www/uploads/avatar',
        'upload_max_filesize' => '10M'
    ),
    'image' => [
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '4M',
        'allowed' => ['image/*'],
        'image_convert' => 'jpg',
        'jpeg_quality' => 50
    ],
    'video' => [
        'lang' => 'ru_RU',
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '4G',
        'allowed' => ['video/*'],
        'trigger' => [
            Uploader::tigger_before => function($uploader) {
        $uploader->api()->file_new_name_body = uniqid($uploader->use . '_');
    },
            Uploader::trigger_after => function($uploader) {
        $api = $uploader->api();
        $path = $api->file_dst_pathname;
        /**
         * @todo полчение информации об видео
         * @param type file
         * @return type array
         */
        $movieInfo = function ($file) {
            if (!is_file($file)) {
                throw new Exception('нет указан файл');
            }

            $movie = new ffmpeg_movie($file);
            $return = [
                //Размеры фильма
                'frame' => [
                    //ширина
                    'width' => $movie->getFrameWidth(),
                    //высота
                    'height' => $movie->getFrameHeight()
                ],
                'duration' => $duration = $movie->getDuration(),
                //продолжительность
                'time' => gmdate("H:i:s", $duration),
                'codec' => [
//            ошибка ffmpeg на debian
//                    'video' => $movie->getVideoCodec(),
                    'audio' => @$movie->getAudioCodec(),
                ],
                'fps' => round($movie->getFrameRate()),
                'rate' => [
                    'audio' => $AudioBitRate = @$movie->getAudioSampleRate(),
//            ошибка ffmpeg на debian
//            'video' => (($VideoBitRate = $movie->getVideoBitRate()) === false) ? $movie->getBitRate() - $AudioBitRate : $VideoBitRate,
                    /**
                     * @todo битрейт видео 
                     * 1 - постояный 
                     * 0 - динамический 
                     */
                    'mode' => (bool) @$VideoBitRate
                ],
                'channel' => ($channel = @$movie->getAudioChannels()) ? $channel : 0,
            ];
            return $return;
        };
        if ($uploader->use === 'video') {
            $info = $movieInfo($path);
        } else {
            $info = [
                'width' => $api->image_src_x,
                'height' => $api->image_src_y,
                'type' => $api->image_src_type,
                'bits' => $api->image_src_bits,
                'pixels' => $api->image_src_pixels,
            ];
        }

        $info['mime'] = $uploader->use;
        $file = strstr($path, pathinfo($path, PATHINFO_EXTENSION), true) . 'json';
        file_put_contents($file, json_encode($info));
        $info['file'] = $api->file_dst_name;
        $info['result'] = true;
        return $info;
    },
        ]
    ]
);
?>
