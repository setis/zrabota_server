<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    'worker' => array(
        'default' => array(
            'driver' => 'pecl',
            'servers' => array(
                '127.0.0.1' => array('127.0.0.1', 4730),
            ),
            'functions' => array(
                'Task_Reverse' => array(
                    'callback' => array('reverse', 'work'),
                    'timeout' => 3600,
                ),
                'Task_Mail_Send' => array(
                    'callback' => array('mail_send', 'work'),
                    'timeout' => 3600,
                ),
            ),
        ),
    ),
    'client' => array(
        'default' => array(
            'driver' => 'pecl',
            'servers' => array(
                array('127.0.0.1', 4730),
            ),
        ),
    ),
);