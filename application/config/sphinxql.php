<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * This file is part of SphinxQL for Kohana.
 *
 * Copyright (c) 2010, Deoxxa Development
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return array(
    'default' => array(
        'servers' => array(
                        'default' => '127.0.0.1:9312'
		),
    ),
);

?>