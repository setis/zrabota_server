<?php

class Task_Mail_Send extends Gearman_Task {

    // Used to override the default, generated, function name.
    public function function_name() {
        return 'mail_send';
    }

    // The guts of what the worker actually does
    public function _work() {
        $result = json_decode($this->workload());
        $this->send_status(1, 3);
        $template = View::factory($result->template, (array) $result)->compile();
        $mailer = new Mailer();
        $mailer->set_message($template);
        $mailer->set_subject('cerf');
        $recipient = new EmailAddress($result->email, 'reg');
        $mailer->add_recipient($recipient);
        $this->send_status(2, 3);
        if ($mailer->send()) {
            $result = true;
        } else {
            $result = Action::error('phpmailer', $mailer->get_error());
        }
        $this->send_status(3, 3);
        $this->send_complete(json_encode($result));
    }

}
?>

