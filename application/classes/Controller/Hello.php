<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of hello
 *
 * @author Lava
 */
class Controller_hello extends Controller {

    function action_mailer() {

        $mailer = new Mailer();

        $mailer->set_message('asdsdfdsafsadfasf');
        $mailer->set_subject('cerf');
        //$sender = new EmailAddress('is.set.dev@gmail.com', 'hsafjhjsd');
        //$mailer->set_sender($sender);
        $recipient = new EmailAddress('setis-0@mail.ru', 'hsafjhjsd');
        $mailer->add_recipient($recipient);
        #$mailer->set_reply_to($sender);
        $mailer->send();
        $this->response->body(print_r($mailer->get_error(), true));
    }

    function action_index() {
        $this->response->body(print_r(get_class_methods(get_class($this)), true));
    }

    function action_sessionWrite() {
        $_SESSION['test'] = time();
        $this->response->body(print_r($_SESSION['test'], true));
    }

    function action_sessionRead() {
        $this->response->body(print_r($_SESSION, true));
    }

    function action_sessionId() {
        $this->response->body(session_id());
    }

    function action_geo() {
        $ip = $_SERVER['REMOTE_ADDR'];

        echo Geoip3::instance()->city($ip) . "<br/>";
        // will return the city name  

        $mode = 'geo';
        echo Geoip3::instance()->coord($ip, $mode) . "<br/>";
        // will return the geographical coords
        // $mode can be one of the following:
        // 'geo-dms' - will return the coords in degree/minute/second format
        // 'geo-dec' - will return the coords in a decimal format 
        // 'geo'     - will return the raw coords

        echo Geoip3::instance()->city_info($ip) . "<br/>";
        // will return a nice formatted string consisting in the city name and 
        //geo-dms coords between brackets

        $property = 'region';
        echo Geoip3::instance()->property($property, $ip) . "<br/>";
        // will retrieve a specified property associated with an ip address 
        // from the maxmind database. to get a list of possible property names,
        // see the geoiprecord class from geoip3/vendor/maxmind/geoipcity.php

        var_dump(Geoip3::instance()->record($ip));
        // returns an object with all the information in the maxmind database
        // related to an ip address, or null
    }

    public function action_factory() {
        $this->factory('Verification')->create(rand(10, 1000), 'register');
    }

    public function action_isauth() {
        var_dump($this->factory('auth')->check());
    }

    public function action_upload() {

        $html = '<form enctype="multipart/form-data" method="post" action="/hello/uploader">
       
            <input type="file" size="32" name="image_field" value="">
            <input type="submit" name="Submit" value="upload">
        </form>';
        $this->response->body($html);
    }

    public function action_uploadN() {

        $html = '<form enctype="multipart/form-data" method="post" action="/hello/uploaderN">
       
            <input type="file" size="32" name="image_field" value="">
            <input type="submit" name="Submit" value="upload">
        </form>';
        $this->response->body($html);
    }

    public function action_uploader() {
        ini_set('upload_max_filesize', '100M');
        //var_dump(Uploader::instance('user')->api());
        $this->response->body(Uploader::instance('user')->exec($_FILES['image_field']));
    }

    public function action_uploaderN() {
//        //$handle = Uploader::instance()->api();
//        $handle = new class_upload($_FILES['image_field']);
////$handle = new upload('php:'.$_SERVER['HTTP_X_FILE_NAME']);
//  if ($handle->uploaded) {
//      $handle->file_new_name_body   = 'image_resized';
//      $handle->image_resize         = true;
//      $handle->image_x              = 100;
//      $handle->image_ratio          = true;
//      $handle->image_ratio_y        = true;
//      $handle->process('/tmp');
//      if ($handle->processed) {
//          echo 'image resized';
//          $handle->clean();
//      } else {
//          echo 'error : ' . $handle->error;
//      }
//  }
        Uploader::instance()->test();
    }

    public function action_reverse($workload = 'Testing') {
        ob_end_flush();

        $client = Gearman_Client::instance('default');
        $job = Gearman::Job('Reverse');
        $job->workload($workload);

        try {
            $result = $client->run_job($job);
            var_dump($result);
        } catch (Exception $e) {
            echo 'Caught Exception: ' . $e->getMessage() . "\n";
        }
    }

    // This method runs a multiple jobs in parallel, in the foreground.
    // Dont forget to run multiple worker instances to see how this really works!
    public function action_reverseset($workloads = 'Testing 1,Testing 2,Testing 3,Testing 4') {
        ob_end_flush();

        $workloads = explode(',', $workloads);

        $client = Gearman_Client::instance('default');

        $jobs = array();

        foreach ($workloads as $workload) {
            $job = Gearman::Job('Reverse');
            $job->workload($workload);

            // $unique = A number that can be used later to pick out this jobs result...
            $unique = $client->add_job($job);
        }

        try {
            /*
             * The array keys of $results will match up with $unique from above.
             */
            $results = $client->run_jobs();

            /*
             * Note: If the Job_Reverse::on_* handlers throw an exception, You'll find
             * a Gearman_Client_Exception in $results[$unique]. The excpetions
             * thrown by the on_* handlers can be found in the
             * $results[$unique]->exceptions array
             */
            var_dump($results);
        } catch (Exception $e) {
            echo 'Caught Exception: ' . $e->getMessage() . "\n";
        }
    }

    public function action_worker() {
        ob_end_flush();

        $worker = Gearman_Worker::instance('default');

        while ($worker->work());
    }

    function gearman_job() {
        $n = rand(0, 1000);
        $login = 'asdsadasd' . $n;
        $email = $login . '@as.sd';
        $password = '123123';
        return $this->factory('Auth')->register($login, $email, $password);
    }

    public function gearman_worker() {
        Gearman::work();
    }

    public function template() {
        $n = rand(0, 1000);
        $login = 'asdsadasd' . $n;
        $email = $login . '@as.sd';
        $password = '123123';
        $view = View::factory('mail/reg', array('login' => $login, 'password' => $password, 'email' => $email));
        return array('result' => $view->compile());
    }

    public function getRole() {
        return ORM::factory('User')->roles();
    }

    public function SphinxQL() {
        return $sphinxql = new Sphinxql();
    }

}

?>
