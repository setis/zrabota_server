<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of verification
 *
 * @author Lava
 */
class Controller_Verification extends Controller {

    protected $cfg;

    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->cfg = kohana::$config->load('verification');
    }

    public function create($id, $action, &$return) {
        $model = ORM::factory('verification');
        $model->key = $this->generate($id);
        $model->user = $id;
        $model->time = date("Y-m-d H:i:s", (time() + $this->cfg['time']));
        $mod = $this->modul('action');
        if ($mod->check($action)) {
            $model->action = $action;
        } else {
            return Action::errorUser('not found action', 'не найдено такое действие');
        }
        try {
            $return->id = $model->create()->id;
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        static $list = array('key','user','time','action');
        foreach ($list as $key) {
            $return->{$key} = $model->{$key};
        }
        return true;
    }

    public function action_index() {
        $this->response->body('вертификация');
    }

    /*
     * проверяем $id пользователя и его $key ключ
     */

    public function check($id, $key, $action) {
        $model = ORM::factory('verification')->where('key', '=', $key)->where('id', '=', $id)->where('action', '=', $action);
        try {
            $result = $model->find()->as_array();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        if ($result['time'] > time()) {

            //превышение хранение ссылки
            return Action::errorUser('session time limit is exceeded', 'превышение хранение ссылки');
        }

        $mod = $this->modul('action');

        if (!$mod->check($action)) {
            return Action::errorUser('mod not check', 'нет такой функци');
        }
        if (!$mod->exec($action, array($result))) {
            return Action::errorUser('mod failed', 'невыполнена');
        }
        $model->delete();
        return true;
    }

    /*
     * удаляем ключ
     */

    public function delete($id) {
        $model = ORM::factory('verification')->where('id', '=', $id);
        $model->delete();
    }

    /*
     * генерация кода доступа
     */

    public function generate($id, $time = null) {
        if ($time === null) {
            $time = time();
        }
        return sha1(md5($time . $this->cfg['salt']) . $id);
    }

}

?>
