<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of msg
 *
 * @author Lava
 */
class Controller_api_msg extends Controller {

    public function info($id) {
        $profile = ORM::factory('profile_personal')->where('id', '=', $id);
        try {
            return $profile->find()->as_array();
        } catch (Database_Exception $e) {
            return Action::errorUserException($e);
        }
    }

}

?>
