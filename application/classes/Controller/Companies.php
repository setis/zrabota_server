<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Companies extends Controller {

    public function create($category = null, $name, $description, $site, $logo) {
        static $list = array(
    'name',
    'description',
    'site',
    'logo'
        );
        $companies = ORM::factory('companies');
        foreach ($list as $var) {
            $companies->text->{$var} = ${$var};
        }
        try {
            $id = (string) $companies->text->create();
            if (!empty($category) && is_numeric($category)) {
                $companies->company_category->category = $category;
                $companies->company_category->company = $id;
                $companies->company_category->create();
            }
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function view($id = null) {
        $companies = ORM::factory('companies');
        if ($id === null || empty($id)) {
            return Arr::fetch($companies->find_all()->as_array());
        } else {
            return $companies->find()->as_array();
        }
    }

    public function edit($id, $category, $name, $description, $site, $logo) {
        static $list = array(
    'name',
    'description',
    'site',
    'logo'
        );

        $companies = ORM::factory('companies', $id);
        foreach ($list as $var) {
            $companies->text->{$var} = ${$var};
        }
        $companies->company_category->category = $category;
        $companies->company_category->company = $id;

        try {
            $companies->text->update();
            $companies->company_category->update();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function delete($id) {
        if (is_numeric($id)) {
            $companies = ORM::factory('companies', $id);
            try {
                $companies->text->delete();
                $companies->company_category->delete();
            } catch (Database_Exception $e) {
                return Action::errorException($e);
            }
            return true;
        }
        return Action::errorUser('not numeric', 'не введен номер компании');
    }

}

?>
