<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of upload
 *
 * @author Lava
 */
class Controller_upload extends Controller {

    public function avatar($mode) {
        if ($mode) {
            if (!isset($_FILES['avatar'])) {
                return Action::errorUser('upload', 'не указен файл');
            }
            $upload = Uploader::instance('avatar');
            $upload->api()->file_new_name_body = Auth::instance()->get_user(null);
            if ($upload->exec($_FILES['avatar'])) {
                return true;
            } else {
                return Action::errorUser('upload', $upload->error());
            }
        } else {
            $html = '<form enctype="multipart/form-data" method="post" action="/action/?mod=upload&action=avatar&in[]=1">
                        <input type="file" size="32" name="avatar" value="">
                        <input type="submit" name="Submit" value="upload">
                    </form>';
            echo $html;
            exit();
        }
    }

    public function logo($mode = false) {
        if ($mode) {
            if (!isset($_FILES['logo'])) {
                return Action::errorUser('upload', 'не указен файл');
            }
            $upload = Uploader::instance('logo');
            if ($upload->exec($_FILES['logo'])) {
                return true;
            } else {
                return Action::errorUser('upload', $upload->error());
            }
        } else {
            $html = '<form enctype="multipart/form-data" method="post" action="/api/?mod=Controller_upload&action=logo&in[]=1">
                        <input type="file" size="32" name="logo" value="">
                        <input type="submit" name="Submit" value="upload">
                    </form>';
            echo $html;
            exit();
        }
    }

    public function image($mode = false) {
        if ($mode) {
            return $this->upload(__FUNCTION__);
        } else {
            self::form(__FUNCTION__);
        }
    }

    public function video($mode = false) {
        if ($mode) {
            return $this->upload(__FUNCTION__);
        } else {
            self::form(__FUNCTION__);
        }
    }

    protected function upload($name) {
        if (!isset($_FILES[$name])) {
            var_dump($_FILES,$name);
            throw new Exception('не указен файл ' . $name);
        }
        $uploader = Uploader::instance($name);
        return $uploader->exec($_FILES[$name]);
    }

    protected static function form($name) {
        $html = '<form enctype="multipart/form-data" method="post" action="/api/?mod=Controller_upload&action=' . $name . '&in[]=1">
                        <input type="file" size="32" name="' . $name . '" value="">
                        <input type="submit" name="Submit" value="upload">
                    </form>';
        echo $html;
        exit();
    }

}

?>
