<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of auth
 *
 * @author Lava
 */
class Controller_auth extends Controller {

    protected $auth;

    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->auth = Auth::instance();
    }

    public function login($login, $password, $remember = FALSE) {
        return $this->auth->login($login, $password, $remember);
    }

    public function action_index() {
        $this->response->body('авторизация');
 }

    public function logout($destroy = FALSE, $logout_all = FALSE) {
        return $this->auth->logout($destroy, $logout_all);
    }

    public function check() {
        return $this->auth->logged_in();
    }

    public function isLogin($in) {
        return (bool) ORM::factory('User')->where('username', '=', $in)->count_all();
    }

    public function isEmail($in) {
        return (bool) ORM::factory('User')->where('email', '=', $in)->count_all();
    }

    public function register($login, $email, $password) {
        try {
            $user = ORM::factory('User')
                    ->create_user(array('username' => $login, 'email' => $email, 'password' => $password), array('username', 'email', 'password'))
                    ->add('roles', ORM::factory('Role', 1000));
        } catch (ORM_Validation_Exception $e) {
            return Action::errorUser('validation', $e->errors());
        }
        $return = new stdClass();
        $return->template = 'mail/reg';
        $status = $this->factory('Verification')->create($user->id, 'register', $return);
        if ($status === true) {
            $return->email = $email;
            $return->login = $login;
            $return->password = $password;
            return Gearman::Exec('mail_send', $return);
        }
        return $status;
    }

    /*
     * ссылка на активацию 
     */

    public function activation($id, $key, $action) {
        return $this->factory('Verification')->check($id, $key, $action);
    }

}

?>
