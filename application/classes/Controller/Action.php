<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of action
 *
 * @author Lava
 */
class Controller_Action extends Controller {

    static protected $import = [
        'request',
        'response'
    ];

    /*
      public function action_index() {
      $action = Action::getInstance();
      $Permission = Permission::init();
      $callback = array($Permission, 'exec');
      foreach(self::$import as $var){
      $action->{$var} = $this->{$var};
      }
      $action->exec($callback);
      }
     */

    public function action_index() {
        $api = new Api();
        array_walk(self::$import, function($var)use(&$api){
            $api->{$var} = &$this->{$var} ;
        });
        $api->run();
    }

}

?>
