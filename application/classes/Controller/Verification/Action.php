<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of verification_action
 *
 * @author Lava
 */
class Controller_verification_action extends Controller {

    public function check($name) {
        return method_exists($this, $name);
    }

    public function register($data) {
        $model = ORM::factory('verification');
        $flag=true;
        foreach($data as $field=>$value){
            if($flag){
                $model->where($field, '=', $value);
                $flag=false;
            }else{
                $model->and_where($field, '=', $value);
            }
        }
        $model->find();
        $model1 = ORM::factory('user',$data['user']);
        try {
            $model1->remove('roles',1000);
            $model1->add('roles', 1001);
            $model->delete();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        
        return true;
    }
    /*
     * смена пароля
     */
    public function password() {
        
    }
    /*
     * вход по ссылки
     */
    public function login(){
        
    }

    public function exec($name, $args) {
        return call_user_func_array(array(&$this, $name), $args);
    }

}

?>
