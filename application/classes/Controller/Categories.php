<?php

defined('SYSPATH') or die('No direct script access.');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Lava
 */
class Controller_Categories extends Controller {

    public function create($name, $parent = null) {
        if (empty($parent) && !is_numeric($parent)) {
            return false;
        }
        $category_text = ORM::factory('category_text');
        $category_text->name = $name;
        $id = (string) $category_text->create();
        $category_this = ORM::factory('category_this');
        $category_this->id = $id;
        $category_this->parent = $parent;
        try {
            $category_this->create();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return array('id' => (string) $id);
    }

    public function view($id = null) {
        if ($id != null) {
            $categories = ORM::factory('categories');
            $result = $categories->where('id', '=', $id)->find()->as_array();
        } else {
            $categories = ORM::factory('categories');
            $arr = $categories->find_all()->as_array();
            foreach ($arr as $i => $value) {
                $result[$i] = $value->as_array();
            }
        }
        return $result;
    }

    public function edit($id, $parent, $name) {
        $categories = ORM::factory('categories', $id);
        $categories->name = $name;
        $categories->this->parent = $parent;
        try {
            $categories->update();
            $categories->this->update();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function delete($id = null) {
        if ($id === null) {
            return false;
        }
        $categories = ORM::factory('categories', $id);
        try {
            $categories->this->delete();
            $categories->text->delete();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

}

?>
