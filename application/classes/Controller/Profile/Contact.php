<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of contact
 *
 * @author Lava
 */
/*
 * для системы и админа
 */
interface contact {

    public function add($user, $type, $contact);

    public function delete($user, $type, $contact);

    public function change($user, $type_search, $contact_search, $type_replace, $contact_replace);
}


class Controller_profile_contact extends Controller implements contact {

    public function add($user, $type, $contact) {
        $model = ORM::factory('profile_contact');
        $model->user = $user;
        $model->type = $type;
        $model->contact = $contact;
        try {
            $model->save();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function change($user, $type_search, $contact_search, $type_replace, $contact_replace) {
        $model = ORM::factory('profile_contact')->where('user', '=', $user)->and_where('type', '=', $type_search)->and_where('contact', '=', $contact_search)->find();
        $model->type = $type_replace;
        $model->contact = $contact_replace;
        try {
            $model->update();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function delete($user, $type, $contact) {
        $model = ORM::factory('profile_contact')->where('user', '=', $user)->and_where('type', '=', $type)->and_where('contact', '=', $contact)->find();
        try {
            $model->delete();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

}


?>
