<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of education
 *
 * @author Lava
 */
interface education {

    public function add($user, $block);

    public function change($user, $block);

    public function delete($user);
}

class Controller_profile_education extends Controller implements education {

    public function add($user, $block) {
        $model = ORM::factory('profile_education');
        $model->profil_id = $user;
        $model->block = $block;
        try {
            $model->save();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function change($user, $block) {
        $model = ORM::factory('profile_education')->where('profile_id', '=', $user)->find();
        $model->block = $block;
        try {
            $model->update();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function delete($user) {
        $model = ORM::factory('profile_education')->where('profile_id', '=', $user)->find();
        try {
            $model->delete();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

}
?>
