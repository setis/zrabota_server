<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of cutaway
 *
 * @author Lava
 */
interface cutaway {

    public function add($company, $user, $comment);

    public function change($company, $user, $comment);

    public function delete($company, $user, $comment);
}

class Controller_profile_cutaway extends Controller implements cutaway {

    public function add($company, $user, $comment) {
        $model = ORM::factory('profile_cutaway');
        $model->company = $company;
        $model->user = $user;
        $model->comment = $comment;
        try {
            $model->save();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function delete($company, $user) {
        $model = ORM::factory('profile_cutaway')->where('company', '=', $company)->and_where('user', '=', $user)->find();
        try {
            $model->delete();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

    public function change($company, $user, $comment) {
        $model = ORM::factory('profile_cutaway')->where('profile_id', '=', $user)->and_where('company', '=', $company)->find();
        $model->comment = $comment;
        try {
            $model->update();
        } catch (Database_Exception $e) {
            return Action::errorException($e);
        }
        return true;
    }

}

?>
