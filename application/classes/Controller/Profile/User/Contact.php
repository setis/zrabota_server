
<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of contact_user
 *
 * @author Lava
 */
interface user_contact {
    /*
     * получение id user
     */

    public function user();

    public function add($type, $contact);

    public function delete($type, $contact);

    public function change($type_search, $contact_search, $type_replace, $contact_replace);
}

class Controller_profile_user_contact extends Controller_profile_contact implements user_contact {

    public $user;

    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->user();
    }
    public function add($type, $contact) {
        return parent::add($this->user, $type, $contact);
    }

    public function change($type_search, $contact_search, $type_replace, $contact_replace) {
        parent::change($this->user, $type_search, $contact_search, $type_replace, $contact_replace);
    }

    public function delete($type, $contact) {
        parent::delete($this->user, $type, $contact);
    }

    public function user() {
        $this->user = Auth_ORM::instance()->get_user(0);
        if(!$this->user){
            Action::json(Action::errorUser('not auth', 'не авторизован'));
            exit();
        }
        return $this->user;
    }

}

?>
