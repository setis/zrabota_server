<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of education
 *
 * @author Lava
 */
interface user_education {

    public function user();

    public function add($block);

    public function change($block);

    public function delete();
}

class Controller_profile_user_education extends Controller_profile_education implements user_education {

    public $user;

    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->user();
    }

    public function user() {
        $this->user = Auth_ORM::instance()->get_user(0);
        if(!$this->user){
            Action::json(Action::errorUser('not auth', 'не авторизован'));
            exit();
        }
        return $this->user;
    }

    public function add($block) {
        return parent::add($this->user, $block);
    }

    public function change($block) {
        return parent::change($this->user, $block);
    }

    public function delete() {
        return parent::delete($this->user);
    }

}

?>
