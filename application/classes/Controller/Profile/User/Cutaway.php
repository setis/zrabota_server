<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of cutaway
 *
 * @author Lava
 */
interface user_cutaway {

    public function user();

    public function add($company, $comment);

    public function change($company, $comment);

    public function delete($company);
}

class Controller_profile_user_cutaway extends Controller_profile_cutaway implements user_cutaway {

    public $user;

    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->user();
    }
    public function add($company, $comment) {
        return parent::add($company, $this->user, $comment);
    }

    public function change($company, $comment) {
        return parent::change($company, $this->user, $comment);
    }

    public function delete($company) {
        return parent::delete($company, $this->user);
    }

    public function user() {
        $this->user = Auth_ORM::instance()->get_user(0);
        if(!$this->user){
            Action::json(Action::errorUser('not auth', 'не авторизован'));
            exit();
        }
        return $this->user;
    }

}

?>
