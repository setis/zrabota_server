<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of enter
 *
 * @author Lava
 */
interface location_via_enter {

    public function add($user, $country, $city, $street, $home);

    public function change($user);

    public function delete();
}

class Controller_location_enter extends Controller {

    static protected $stepList = array(
        '0' => 'country',
        '1' => 'city'
    );
    static protected $min,$max;
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        if(self::$min===null){
            self::$min = min(self::$stepList);
        
        }
        if(self::$max===null){
            self::$max = max(self::$stepList);
        }
    }

    public function step($step, $data = null) {
        if(is_numeric($step)){
            return false;
        }
        if(($step-1) !== count($data)){
            return false;
        }
        if(self::$min<$step && $step>self::$max){
            return false;
        }
        $result = array();
        for ($s = 0; $s < $step; $s++) {
            $name = self::$stepList[$step];
            $result[$name]=$this->{'get_'.$name}($data[$name]);
        }
        return $result;
    }


}

?>
