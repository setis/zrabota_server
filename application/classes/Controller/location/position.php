<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of position
 *
 * @author Lava
 */
interface location_via_position {

    public function add($user, $country, $city, $street, $home);

    public function change();

    public function delete();
}

class Controller_location_position extends Controller {
    protected $geoip;
    public function __construct(\Request $request, \Response $response) {
        parent::__construct($request, $response);
        $this->geoip = new geoip_maxmind_local();
        
    }
    public function test(){
        $this->geoip->test();
    }
}

interface faceGeoip {

    public function geoip_record_by_name(string $host);
}

class geoip {
    
}

class geoip_maxmind_local extends geoip {

    public function test() {
        $info = geoip_record_by_name($_SERVER['REMOTE_ADDR']);
        print_r($info);
    }

}

?>
