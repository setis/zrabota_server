<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of company
 *
 * @author Lava
 */
class Model_companies extends ORM {

    protected $_table_name = 'companies';
    protected $_has_one = array(
        'company_category' => array(
            'model' => 'company_category',
            'foreign_key' => 'company',
        ),
        'text' => array(
            'model' => 'company_text',
            'foreign_key' => 'id'
        )
    );
    

}

?>
