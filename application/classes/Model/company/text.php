<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Lava
 */
class Model_Company_Text extends ORM {

    protected $_table_name = 'company_text';
    protected $_has_one = array(
        'category' => array(
            'model' => 'company_category',
            'foreign_key' => 'company',
        ),
        'companies' => array(
            'model' => 'companies',
            'foreign_key' => 'id',
        )
    );

}

?>
