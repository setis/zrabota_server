<?php

defined('SYSPATH') or die('No direct script access.');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Lava
 */
class Model_Company_Category extends ORM {

    protected $_table_name = 'company_category';
    protected $_has_one = array(
        'text' => array(
            'model' => 'company_text',
            'foreign_key' => 'id',
            'for_key'=>'company'
        ),
        'companies' => array(
            'model' => 'companies',
            'foreign_key' => 'id',
            'for_key'=>'company'
        )
    );

}

?>
