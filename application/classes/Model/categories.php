<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of categories
 *
 * @author Lava
 */
class Model_Categories extends ORM{
    protected $_table_name = 'categories';
    protected $_has_one = array(
        'this' => array(
            'model' => 'category_this',
            'foreign_key' => 'id'
        ),
        'text' => array(
            'model' => 'category_text',
            'foreign_key' => 'id'
        )
    );

}

?>
