<?php defined('SYSPATH') or die('No direct script access.');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Lava
 */
class Model_Category_Text extends ORM{
    protected $_table_name = 'category_text';
    protected $_has_one = array(
        'this' => array(
            'model' => 'category_this',
            'foreign_key' => 'id'
        ),
        'categories' => array(
            'model' => 'categories',
            'foreign_key' => 'id'
        )
    );
    
}

?>
