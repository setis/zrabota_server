<?php

defined('SYSPATH') or die('No direct script access.');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Lava
 */
class Model_Category_This extends ORM {

    protected $_table_name = 'category_this';
    protected $_has_one = array(
        'text' => array(
            'model' => 'category_text',
            'foreign_key' => 'id'
        ),
        'categories' => array(
            'model' => 'categories',
            'foreign_key' => 'id'
        )
    );

}

?>
