<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Description of user
 *
 * @author Lava
 */
class Permission_Guest extends Permission {

    public $order = true;

    public function class_user() {
        return true;
    }

    public function user_activator() {
        return true;
    }

    public function class_auth() {
        return true;
    }

    public function auth_islogin() {
        return true;
    }

}

?>
