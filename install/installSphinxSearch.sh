wget http://sphinxsearch.com/files/sphinxsearch_2.0.8-release-1_amd64.deb
dpkg -i sphinxsearch_2.0.8-release-1_amd64.deb
indexer --config /etc/sphinxsearch/default_sphinx.conf --all
crontab -e  0 1 * * * indexer --config /etc/sphinxsearch/default_sphinx.conf --rotate