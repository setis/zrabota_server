git clone https://github.com/jackeylu/mysql2redis.git
cd mysql2redis
git clone  https://github.com/redis/hiredis.git
cd hiredis
make
make install 
cd ..
apt-get install ruby rubygems
gem install hiredis

git clone https://github.com/alexfok/redis_udf.git
cd redis_udf
wget http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.11.tar.gz/from/http://cdn.mysql.com/
mv index.html mysql-5.6.11.tar.gz
tar -zxf mysql-5.6.11.tar.gz
gcc -shared -o redis_udf.so redis_udf.c  -I "./mysql-5.6.11/include" -I "/usr/include/mysql" -fPIC libhiredis.a
cp redis_udf.so /usr/lib64/mysql/plugin
mysql -u root -p
CREATE FUNCTION redis_set RETURNS INTEGER SONAME 'redis_udf.so';
CREATE FUNCTION redis_servers_set RETURNS INTEGER SONAME 'redis_udf.so';
