<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_User extends Model_Auth_User {

	// This class can be replaced or extended
     public function hash_password($value)
    {
        return ($value !== '' AND UTF8::strlen($value) >= 5) ? Auth::instance()->hash($value) : $value;
    }

} // End User Model