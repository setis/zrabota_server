<?php

defined('SYSPATH') OR die('No direct script access.');
define('PERMISSION_PATH', APPPATH . 'classes' . DIRECTORY_SEPARATOR . 'Permission' . DIRECTORY_SEPARATOR);
define('PERMISSION_LOG_PATH', APPPATH . 'logs' . DIRECTORY_SEPARATOR . 'permission');

class Permission {

    public $obj, $func, $args;
    public $name, $class;
    protected $action;
    /*
     * по умолчанию доступ
     * true - есть ,false - нету  
     */
    public $order = true;
    static public $default = 'guest';
    static public $import = array(
        'obj',
        'func',
        'args',
        'class'
    );
    static public $mode = 1;
    protected $log = array();

    const DENY = 0;
    const ALLOW = 1;

    public function exec(&$action) {
        $this->action = $action;
        foreach (self::$import as $var) {
            $this->{$var} = &$action->{$var};
        }
        $result = $this->permission();
        if (!self::$mode) {
            return $result;
        } else {
            if (!$result) {
                $this->not_access($this->log);
            }
            return true;
        }
    }

    /*
     * проверка текущий правило является по умолчанию
     */

    public function isCheckDefault() {
        return (self::$default === $this->name());
    }

    public static function not_access($log = null) {
        $result = array(
            'result' => false,
            'error' => array(
                'code' => 'access deny',
                'access' => $log
            )
        );

        Api::response($result, array('HTTP/1.1 403 Forbidden'));
    }

    public function name() {
        return str_replace('Permission_', '', get_class($this));
    }

    /*
     * проверка доступа
     */

    protected function permission() {
        $checkList = array(
            'class' => 'class_' . $this->class,
            'func' => $this->class . '_' . $this->func,
            'args' => 'args_' . $this->class . '_' . $this->func
        );
        $check = function ($check, &$self) {
                    if (!method_exists($self, $check)) {
                        return $self->order;
                    }
                };
        $this->log = array();
        $result = true;
        foreach ($checkList as $name => $method) {
            $status = $check($method, $this);
            if ($status === null) {
                $status = $this->{$method}();
                if (!$status) {
                    $result = false;
                }
            }
            //echo $this->name() . ":$name->" . (int) $status . "<br>";
            $this->log[$name]['status'] = $status;
            $this->log[$name]['method'] = $this->{$name};
            if(!$result){
                break;
            }
        }
        $this->log['permission'] = $this->name();

        return $result;
    }

    public static function log($code, $msg, array $values = NULL, array $additional = NULL) {
        Kohana::$log->attach(new Log_File(PERMISSIONLOGSPATH));
        Kohana::$log->add($code, $msg, $values, $additional)->write();
    }

    public static function init($notfoundDefault = true, $default = true) {
        //$roles = ORM::factory('User')->roles();
        
        if (isset($_SESSION['permission'])) {
            $path = PERMISSIONPATH . $_SESSION['permission'] . '.php';
            $class = 'Permission_' . $_SESSION['permission'];
            try {
                require $path;
                return new $class();
            } catch (Kohana_Exception $e) {
                self::log(Log::INFO, 'not found permission', NULL, $e);
                if (!$notfoundDefault && self::$default === null) {
                    return false;
                } else {
                    if (!$default && self::$default === null) {
                        return false;
                    } else {
                        return self::_default();
                    }
                }
            }
        } else {
            if (!$default && self::$default === null && !method_exists(__CLASS__, self::$default)) {
                self::log(log::CRITICAL, "not set  default permission(не установлены привилегии по умолчанию) ->" . self::$default);
                return false;
            } else {
                return self::_default();
            }
        }
    }

    public static function _default() {
        try {
            $path = PERMISSIONPATH . self::$default . '.php';
            //$path = Kohana::find_file(PERMISSIONPATH, self::$default);
            require $path;
            $class = 'Permission_' . self::$default;
            return new $class();
        } catch (Kohana_Exception $e) {
            //self::log(log::CRITICAL, "not found default permission(не найдены привилегии по умолчанию) ->" . self::$default, null, $e);
            return false;
        }
    }

}

?>
