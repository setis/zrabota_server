<?php

defined('SYSPATH') or die('No direct script access.');
/**
 * Gearman Queue Manager
 *
 * @package    Gearman
 * @author     Kiall Mac Innes
 * @copyright  (c) 2010 Kiall Mac Innes
 * @license    http://kohanaframework.org/license
 */
define('GEARMAN_ROOT', APPPATH . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Gearman' . DIRECTORY_SEPARATOR);
define('GEARMAN_TASK', GEARMAN_ROOT . 'Task' . DIRECTORY_SEPARATOR);
define('GEARMAN_JOB', GEARMAN_ROOT . 'Job' . DIRECTORY_SEPARATOR);

class Gearman {

    const PRIORITY_LOW = '0';
    const PRIORITY_NORMAL = '1';
    const PRIORITY_HIGH = '2';
    const PREFIX_JOB = 'Job_';
    const PREFIX_TASK = 'Task_';
    const Client_Default = 'default';

    static function Job($job) {
        include GEARMAN_JOB . strtolower($job) . EXT;
        $class = static::PREFIX_JOB . $job;
        return new $class;
    }

    static function Task($task) {
        include GEARMAN_TASK . strtolower($task) . EXT;
        $class = static::PREFIX_TASK . $task;
        return new $class;
    }

    static function Client($name = null) {
        if ($name === null) {
            $name = static::Client_Default;
        }
        return Gearman_Client::instance($name);
    }

    static function Exec($job, $data = null, $client = null, $priority = Gearman ::PRIORITY_NORMAL, $async = true) {
        $objClient = Gearman::Client($client, $priority);
        $objJob = Gearman::Job($job);
        $objJob->workload(json_encode($data));
        try {
            /*
             * run_job_bg   async
             * run_job      sync
             */
            if ($async) {
                $result = $objClient->run_job_bg($objJob);
            } else {
                $result = $objClient->run_job($objJob);
            }
            return json_decode($result);
        } catch (Exception $e) {
            return Action::errorException($e);
        }
    }

    static function worker($name = null) {
        return Gearman_Worker::instance($name);
    }

    static function work($name = null) {
        ob_end_flush();
        $worker = self::worker($name);
        while ($worker->work());
    }

}
