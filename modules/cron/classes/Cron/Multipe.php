<?php

class Cron_Multipe implements iConfigHandler {

    public
            $db,
            $file;

    use ConfigHandler;

    public static $record = [
        'change', 'create', 'delete'
    ];

    public function __construct() {
        $this->init();
    }

    public function init() {
        $this->db = new Cron_Db_Multiple();
        $this->file = new Cron_File();
        $this->bindConfig(kernel::cfg()->db);
        $this->getConfig();
    }

    public function keyConfig() {
        return ['file'];
    }

    public function handlerConfig($mode, $key, $value = null) {
        switch ($key) {
            case 'file':
                if ($mode) {
                    $this->file->setUser($value);
                } else {
                    return $this->file->user;
                }

                break;
            case 'db':
                if ($mode) {
                    $this->db->setConfig($value);
                } else {
                    return $this->db->getConfig;
                }

                break;

            default:
                if ($mode) {
                    $this->config[$key] = $value;
                } else {
                    return $this->config[$key];
                }
                break;
        }
    }

    public function record() {
        $list = $this->db->select();
        $tasks = '';
        array_walk($list, function($value)use(&$tasks) {
            $tasks.=$value['event'] . "\t" . $value['command'] . "\n";
        });
        $this->file->write($tasks);
    }

    public function __call($name, $arguments) {
        if (!method_exists($this, $name) && method_exists($this->db, $name)) {
            $result = call_user_func_array([$this->db, $name], $arguments);
            if (in_array($name, self::$record)) {
                $this->record();
            }
            return $result;
        }
    }

    public function defaultConfig() {
        
    }

}
