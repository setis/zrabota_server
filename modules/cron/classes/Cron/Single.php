<?php

class Cron_Single {

    public
            $db,
            $file;

    use Config;

    public function __construct() {
        $this->init();
    }

    public function init() {
        $this->db = new Cron_Db_Single();
        $this->file = new Cron_File();
        $this->bindConfig(kernel::cfg()->cron);
        $this->getConfig();
    }

    public function defaultConfig() {
        return kernel::cfg()->cron;
    }

    public function task() {
        return '* * * * *' . "\t" . $this->config['cron']['php'] . ' ' . $this->config['cron']['path'] . "\n";
    }

    public function install() {
        $this->file->write($this->task());
        return $this;
    }

    public function uninstall() {
        $data = $this->file->read();
        $replace = str_replace($this->task(), '', $data);
        $this->file->write($replace);
        return $this;
    }

    public function __call($name, $arguments) {
        if (!method_exists($this, $name) && method_exists($this->db, $name)) {
            return call_user_func_array([$this->db, $name], $arguments);
        }
    }

}