<?php

class Cron_Handler_Single implements iConfig {

    public
            $db;
    public
            $pipe = [];

    use Config;

    public function __construct() {
        $this->setConfig_init = true;
        $this->init();
    }

    public function init() {
        $this->db = new Cron_Db_Single();
        $this->db->bindConfig(kernel::cfg()->cron['db']);
        $this->getConfig();
    }

    /**
     * @todo многопоточный запуск приложений без возможностью монитронига
     * не до работан надо изменить базу и $task['argv'] array аргументов
     * pcntl_exec('$task['path']', $task['argv'], $envs);
     */
    public function exec() {
        $tasks = $this->db->task();
        if (count($tasks) === 0) {
            return;
        }
        $envs = (isset($this->config['exec'])) ? $this->config['exec'] : null;
        array_walk($tasks, function($task) use($envs) {
            var_dump($task, $envs);
//                    pcntl_exec('/bin/sh', $task['command'], $envs);
            pcntl_exec('/usr/bin/php', $task['command'], $envs);
        });
    }

    /**
     * @todo многопоточный запуск приложений c возможностью монитронига 
     *  нет огранечение по поточности так что пока огранечение идет 
     * через limit(5) в Cron_Db_Single::task
     */
    public function theard() {
        $tasks = $this->db()->task();
        if (count($tasks) === 0) {
            return;
        }
        $this->pipes = [];
        $parentPid = posix_getpid();
        pcntl_sigprocmask(SIG_BLOCK, array(SIGUSR1, SIGUSR2));
        $config = &$this->config['thread'];
        array_walk($tasks, function($value)use($parentPid, $config) {
            $pid = pcntl_fork();
            $id = &$value['id'];
            if ($pid === 0) {
                $process = proc_open($config['cwd'], $config['descriptorspec'], $this->pipes[$id]);
                if (is_resource($process)) {
                    echo "childer goto parent -> db\n";
                    posix_kill($parentPid, SIGUSR1);
                }
                proc_close($process);
            } elseif ($pid > 0) {
                pcntl_sigwaitinfo(array(SIGUSR1));
                $this->pipes[$pid] = true;
                if (!isset($db) || !$db instanceof Cron_Db_Single) {
                    $db = $this->db();
                }
                $db->success($id);
                posix_kill($pid, SIGUSR2);
                echo "parent goto childer -> proc_close $id\n";
            }
        });
    }

    public function defaultConfig() {
        return kernel::cfg()->cron;
    }

}
