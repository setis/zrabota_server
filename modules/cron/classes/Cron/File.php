<?php

class Cron_File implements iCrontab {

    public $user, $path, $options;

    public function __construct($user = null, $options = null) {
        $this->setUser($user);
        if ($options) {
            $this->options = $options;
        }
    }

    public function setUser($id = null) {
        if ($id === null) {
            $this->user = posix_getpwuid(posix_getuid())['name'];
        } elseif (is_int($id) || is_numeric($id)) {
            $this->user = posix_getpwuid($id)['name'];
        } else {
            $this->user = $id;
        }
        $this->path();
    }

    public static function createHeader($options = null) {
        $data = '';
        if ($options === null) {
            return $options;
        }
        array_walk($options, function($value, $key)use(&$data) {

            $data.=strtoupper($key) . '=' . $value . "\n";
        });
        return $data;
    }

    public function path() {
        return $this->path = self::dir . DIRECTORY_SEPARATOR . $this->user;
    }

    public function write($data) {
        $dir = dirname($this->path);

        if (!is_dir($dir)) {
            throw new Cron_File_Exception('нет такой директории ' . $dir);
        }
        if (!is_file($this->path)) {
            touch($this->path);
        }
        if (!is_writable($this->path)) {
            throw new Cron_File_Exception('нет доступа для записи ' . $this->path);
        }
        if (is_array($data)) {
            $data = implode("\n", $data);
        } else {
            $data.="\n";
        }

        file_put_contents($this->path, self::createHeader($this->options) . self::head . "\n" . $data);
        chown($this->path, $this->user);
        chgrp($this->path, 'crontab');
        chmod($this->path, 0600);
    }

    public function read() {
        $dir = dirname($this->path);
        if (!is_dir($dir)) {
            throw new Cron_File_Exception('нет такой директории ' . $this->path);
        }
        if (!is_file($this->path)) {
            echo $this->path;
            return '';
        }
        if (!is_readable($this->path)) {
            throw new Cron_File_Exception('нет нет доступа для чтения ' . $this->path);
        }
        return file_get_contents($this->path);
    }

}

