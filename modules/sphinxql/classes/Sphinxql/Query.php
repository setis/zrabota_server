<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This file is part of Sphinxql for Kohana.
 *
 * Copyright (c) 2010, Deoxxa Development
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package kohana-sphinxql
 */

/**
 * Wrapper class for Kohana_Sphinxql_Query
 *
 * @package kohana-sphinxql
 * @author MasterCJ <mastercj@mastercj.net>
 * @version 0.1
 * @license http://mastercj.net/license.txt
 */
class Sphinxql_Query extends Kohana_Sphinxql_Query { }

?>