<?php

defined('SYSPATH') OR die('No direct script access.');

define('ACTION_PATH', APPPATH . 'classes' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR);
define('ACTION_CLASS_PREFIX', 'Controller_');

class Action {

    private static $instance;
    /*
     * метод вывода данных
     */
    public static $out = 'json';
    /*
     * обязательный параметр при обработки если его нету $proccess['obj'],
     * то выполнение прирывается
     * 
     */
    public static $module = true;
    /*
     * зависимость есть ли $mod выполняется ,
     * а если нету вызывает ошибку
     */
    public static $dependence = false;
    /*
     * какие переменные обрабатываются
     */
    public static $proccess = array(
        'obj' => array(
            'name' => 'mod',
            'method' => 'GET'),
        'func' => array(
            'name' => 'action',
            'method' => 'GET'),
        'args' => array(
            'name' => 'in',
            'method' => 'REQUEST')
    );
    public static $dependenceList = array('obj', 'func');
    public $obj, $class, $func, $args;

    /**
     * @var  Request  Request that created the controller
     */
    public $request;

    /**
     * @var  Response The response that will be returned from controller
     */
    public $response;

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();
            return self::$instance;
        } else {
            return self::$instance;
        }
    }

    /*
     * метод вывода данных json
     */

    public static function json($result, array $headers = null) {
        if (Kohana::$environment != Kohana::DEVELOPMENT) {
            ob_clean();
        }
        if (is_array($result) || is_object($result)) {
            echo json_encode($result);
        } else {
            echo json_encode(array('result' => $result));
        }
        if (!headers_sent()) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST');
            header('Access-Control-Max-Age: 1000');
            header('Content-Type: application/json; charset=utf-8');
            if ($headers) {
                foreach ($headers as $header) {
                    header($header);
                }
            }
            exit;
        }
    }

    /*
     * метод вывода данных jsonp
     */

    public static function jsonp($result, array $headers = null) {
        if (Kohana::$environment != Kohana::DEVELOPMENT) {
            ob_clean();
        }
        if (is_array($result) || is_object($result)) {
            $result = json_encode($result);
        } else {
            $result = json_encode(array('result' => $result));
        }
        echo $_GET['callback'] . '(' . $result . ')';
        if (!headers_sent()) {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: POST');
            header('Access-Control-Max-Age: 1000');
            header('Content-Type: text/javascript; charset=utf-8');
            if ($headers) {
                foreach ($headers as $header) {
                    header($header);
                }
            }
            exit;
        }
    }

    public function exec($callback = null) {

        $get = function($method) {
                    switch (strtolower($method)) {
                        case 'get':
                            $input = &$_GET;
                            break;
                        case 'post':
                            $input = &$_POST;
                            break;
                        case 'request':
                            $input = &$_REQUEST;
                            break;
                    }
                    return $input;
                };
        foreach (self::$proccess as $var => $array) {
            $input = $get($array['method']);
            if (isset($input[$array['name']])) {
                $this->{$var} = $input[$array['name']];
            }
        }

        foreach (self::$dependenceList as $depend) {
            if (empty($this->{$depend})) {
                exit("not input $depend");
            }
        }
        $this->class = ucfirst($this->obj);
        $class = ACTIONCLASSPREFIX . $this->class;
        if (class_exists($class)) {
            $this->obj = new $class($this->request, $this->response);
        } else {
            $path = ACTIONPATH . $this->obj . '.php';
            include $path;
            $this->obj = new $class($this->request, $this->response);
        }

        if (!method_exists($this->obj, $this->func)) {
            echo "not func";
            if (!headers_sent()) {
                header('HTTP/1.1 404 Not Found');
                exit;
            }
        }
        if (is_callable($callback)) {
            /*
             *  включена проверка на доступ выполнения
             *  $Permission = Permission::init();
             *  $callback = array($Permission, 'exec');
             *  обрабатываем есть ли доступ на выполнение 
             *  если нету то прекращается выполняться скрипт
             */
            $result = call_user_func_array($callback, array(&$this));
            if (!$result) {
                exit;
            }
        }

        if ($this->args != null) {
            try {
                $result = call_user_func_array(array(&$this->obj, $this->func), $this->args);
            } catch (ErrorException $e) {
                $result = self::errorException($e);
            }
        } else {
            try {
                $result = call_user_func(array(&$this->obj, $this->func));
            } catch (ErrorException $e) {
                $result = self::errorException($e);
            }
        }
        self::response($result);
    }

    public static function response($result, array $headers = null) {
        if (isset($_GET['callback'])) {
            self::$out = 'jsonp';
        }
        if (self::$out) {
            forward_static_call_array(array(__CLASS__, self::$out), array($result, $headers));
        } else {
            echo $result;
        }
    }

    public static function errorException(Exception $e, $array = array()) {
        return (array) $array + array(
            'result' => false,
            'error' => array(
                'type' => 'system',
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile()
            )
        );
    }

    public static function errorUserException(Exception $e, $array = array()) {
        return (array) $array + array(
            'result' => false,
            'error' => array(
                'type' => 'user',
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile()
            )
        );
    }

    public static function errorUser($code, $msg) {
        return array(
            'result' => false,
            'error' => array(
                'type' => 'user',
                'code' => $code,
                'msg' => $msg
            )
        );
    }

    public static function error($code, $msg) {
        return array(
            'result' => false,
            'error' => array(
                'type' => 'system',
                'code' => $code,
                'msg' => $msg
            )
        );
    }

}

?>
