<?php

class Api implements iApi {

    public
            $class,
            $func,
            $args;
    public $obj;
    public
            $dependence,
            $dependenceList,
            $proccess,
            $protocol,
            $module,
            $method,
            $json,
            $jsonp,
            $callback,
            $environment,
            $autoloader,
            $dynamic;
    public $error = false;
    public $request, $response;

    public function __construct() {
        $this->init();
    }

    public function init() {
        $cfg = $this->config();
        array_walk($cfg, function($value, $var) {
            $this->{$var} = $value;
        });
    }

    public function config() {
        return kohana::$config->load('api');
    }

    /*
     * ����� ������ ������ json
     */

    public function json($result, array $headers = null) {
        return [
            'body' => self::toJson($result),
            'headers' => ($headers !== null && is_array($headers) && count($headers) > 0) ? $headers + $this->json : $this->json
        ];
    }

    public function response($headers, $body, $exit = true) {
        if ($this->environment != self::DEVELOPMENT) {
            ob_clean();
        }
        echo $body;
        if (!headers_sent()) {
            if (is_array($headers)) {
                $headers = array_unique($headers);
                array_walk($headers, function($header) {
                    header($header);
                });
            } elseif (is_string($headers)) {
                header($headers);
            }
        }
        if ($exit) {
            exit();
        }
    }

    public static function toJson(&$result) {
        if (is_array($result) || is_object($result)) {
            return json_encode($result);
        } else {
            return json_encode(['result' => $result]);
        }
    }

    /*
     * метод вывода данных jsonp
     */

    public function jsonp($result, array $headers = null) {
        return [
            'body' => $this->callback(self::toJson($result)),
            'headers' => ($headers !== null && is_array($headers) && count($headers) > 0) ? $headers + $this->jsonp : $this->jsonp
        ];
    }

    /**
     * @todo захват данных определеных которые приходят на сервер
     * @param type string get|post|request
     * @return type array
     */
    public function &method($method) {
        $method = strtolower($method);
        switch_method:
        switch ($method) {
            case 'get':
            case self::Get:
                return $_GET;
                break;
            case 'post':
            case self::Post:
                return $_POST;
                break;
            case 'request':
            case self::Request;
                return $_REQUEST;
                break;
        }
        $method = &$this->method;
        goto switch_method;
    }

    public function handler($callback = null) {

        array_walk($this->proccess, function($array, $var) {
            $input = $this->method($array['method']);
            if (isset($input[$array['name']])) {
                $this->{$var} = $input[$array['name']];
            }
        });
        array_walk($this->dependenceList, function($depend) {
            if (empty($this->{$depend})) {
                throw new Exception('не введено поле ' . $depend);
            }
        });
        $class = &$this->class;
        if (!class_exists($this->class)) {

//            if ($this->autoloader) {
//                $this->obj = $this->autoloader($this->class);
//            } else {
//                $this->obj = new $this->class();
//            }

        }
        if (is_subclass_of($class, 'Kohana_Controller')) {
            $this->obj = new $class($this->request, $this->response);
        } else {
            $this->obj = new $class();
          
        }
        if (!is_object($this->obj)) {
            throw new Exception;
        }

        if (!$this->dynamic && !method_exists($this->obj, $this->func)) {
            throw new Exception('нет такой метода ' . $this->class . '::' . $this->func);
        }

        if ($this->args != null) {
            $result = call_user_func_array([&$this->obj, $this->func], $this->args);
        } else {
            $result = call_user_func([&$this->obj, $this->func]);
        }
        $result = $this->{$this->protocol}($result);
        $this->response($result['headers'], $result['body']);
    }

    public static function Exception(Exception $e, array $array = null, $trace = false) {
        $result = [
            'result' => false,
            'error' => [
                'exception' => get_class($e),
                'code' => $e->getCode(),
                'msg' => $e->getMessage(),
                'line' => $e->getLine(),
                'file' => $e->getFile(),
                'trace' => ($trace) ? $e->getTrace() : null
            ]
        ];
        if ($array !== null) {
            $result = $result + $array;
        }
        return $result;
    }

    public function run() {
        try {
            $this->handler();
        } catch (Exception $e) {
            $result = $this->{$this->protocol}(self::Exception($e, null, $this->environment));
            $result['headers'][] = 'HTTP/1.1 404 Not Found';
            $this->response($result['headers'], $result['body']);
        }
    }

}

?>
