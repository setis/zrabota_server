<?php

interface iApi {

    const DEVELOPMENT = 1;
    const PRODUCTION = 0;
    const PREFIX = '';
    const
            Get = 0,
            Post = 1,
            Request = 2;

}
