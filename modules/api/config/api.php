<?php

return [
    'dependence' => false,
    'dependenceList' => [
        'class',
        'func'
    ],
    'method' => iApi::Request,
    'proccess' => [
        'class' => [
            'name' => 'mod',
            'method' => 'GET'],
        'func' => [
            'name' => 'action',
            'method' => iApi::Get],
        'args' => [
            'name' => 'in',
            'method' => 'REQUEST']
    ],
    'protocol' => 'json',
    'module' => true,
    'json' => [
        'Access-Control-Allow-Origin: *',
        'Access-Control-Allow-Methods: POST',
        'Access-Control-Max-Age: 1000',
        'Content-Type: application/json; charset=utf-8'
    ],
    'jsonp' => [
        'Access-Control-Allow-Origin: *',
        'Access-Control-Allow-Methods: POST',
        'Access-Control-Max-Age: 1000',
        'Content-Type: text/javascript; charset=utf-8'
    ],
    'callback' => function($json) {
return $_GET['callback'] . '(' . $json . ')';
},
    'environment' => iApi::DEVELOPMENT,
    'autoloader' => function($class) {
return new $class();
},
    'dynamic' => true
];
