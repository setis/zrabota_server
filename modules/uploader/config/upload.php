<?php

return array(
    'user' => array(
        'lang' => 'ru_RU',
        'image_resize' => true,
        'image_x' => 100,
        'image_ratio_y' => true,
        'dir' => '/var/www/uploads',
        'upload_max_filesize' => '1M'
    )
);
?>
