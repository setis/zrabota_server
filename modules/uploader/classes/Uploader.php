<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Upload
 *
 * @author Lava
 */
class Uploader {

    //put your code here
    protected static $_instance;
    protected $cfg, $api;
    protected $lang, $dir;
    static protected $keys = array(
        'lang',
        'dir',
        'upload_max_filesize',
        'trigger'
    );
    public $trigger;

    const tigger_before = 0,
            trigger_after = 1;

    public $use;

    public function __construct($name = null) {
        if (self::$_instance === null) {
            $this->cfg = Kohana::$config->load('upload')->as_array();
            if (!class_exists('clupload')) {
                $path = Kohana::find_file('classes', 'clupload');
                include $path;
            }
            $this->api = new clupload();
            if ($name !== null) {
                $this->select($name);
            }
            self::$_instance = $this;
        }
    }

    public static function instance($name = null) {
        if (self::$_instance === null) {
            return new self($name);
        }
        return self::$_instance->select($name);
    }

    public function select($name) {
        if ($this->use === $name) {
            return $this;
        }
        $this->clean();
        if (isset($this->cfg[$name])) {
            foreach ($this->cfg[$name] as $key => $value) {
                if (in_array($key, self::$keys)) {
                    if (method_exists($this, $key)) {
                        call_user_func(array(&$this, $key), $value);
                    } else {
                        $this->{$key} = $value;
                    }
                } else {
                    $this->api->{$key} = $value;
                }
            }
        }
        if ($name && !isset($this->cfg[$name])) {
            throw new Exception('не указан тип конфигурации загрузки ' . $name);
        }
        $this->use = $name;
        return $this;
    }

    public function __call($name, $arguments) {
        if (!method_exists($this, $name)) {
            return call_user_func_array(array(&$this, $name), $arguments);
        }
    }

    public function lang($lang) {
        $this->api->setLang($lang);
    }

    public function upload_max_filesize($size) {
        ini_set("upload_max_filesize", $size);
        $this->api->file_max_size($size);
    }

    /*
      public function __get($name) {
      return $this->api->{$name};
      }
      public function __set($name, $value) {
      return $this->api->{$name} = $value;
      }
     * 
     */

    public function api() {
        return $this->api;
    }

    public function upload($file, $lang = null) {
        if ($lang === null) {
            $lang = $this->lang;
        }
        return $this->api->upload($file, $lang);
    }

    public function exec($name, $dir = null) {
        if ($this->trigger !== null && is_callable($this->trigger[self::tigger_before])) {
            $this->trigger[self::tigger_before]($this);
        }
        $this->upload($name);
        $this->process($dir);
        if ($this->api->processed) {
            if ($this->trigger !== null && is_callable($this->trigger[self::trigger_after])) {
                $result = $this->trigger[self::trigger_after]($this);
                $this->api->clean();
                return $result;
            } else {
                $this->api->clean();
                return true;
            }
        } else {
            throw new Exception($this->error());
        }
    }

    protected function clean() {
        array_walk(self::$keys, function($var) {
            $this->{$var} = null;
        });
    }

    public function error() {
        return $this->api->error;
    }

    public function process($dir = null) {
        if ($dir === null) {
            $dir = $this->dir;
        }
        return $this->api->process($dir);
    }

    public function test() {
        $handle = new clupload($_FILES['image_field'], 'ru_RU');
//$handle = new upload('php:'.$_SERVER['HTTP_X_FILE_NAME']);
        if ($handle->uploaded) {
            $handle->file_new_name_body = 'image_resized';
            $handle->image_resize = true;
            $handle->image_x = 100;
            $handle->image_ratio = true;
            $handle->image_ratio_y = true;
            $handle->process('/tmp');
            if ($handle->processed) {
                echo 'image resized';
                $handle->clean();
            } else {
                echo 'error : ' . $handle->error;
            }
        }
    }

}

?>
