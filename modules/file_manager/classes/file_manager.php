<?php

class file_manager {

    public static function files_ext($dir, $ext) {
        if (!is_dir($dir)) {
            throw new Exception('не указан директория ' . $dir);
        }
        $dir = realpath($dir) . DIRECTORY_SEPARATOR;
        if (is_array($ext)) {
            $result = '*.{';
            $result.=implode(',', $ext) . '}';
        } else {
            $result = '*.{' . $ext . '}';
        }
        return glob($dir . $result, GLOB_BRACE);
    }

    public static function truncation_dir($dir, &$array) {
        if (!is_dir($dir)) {
            throw new Exception('не указан директория ' . $dir);
        }
        $dir = realpath($dir) . DIRECTORY_SEPARATOR;
        $result = [];
        array_walk($array, function($value)use(&$result, &$dir) {
            $result[] = str_replace($dir, '', $value);
        });
        return $result;
    }

}

class core {

    static protected $_instance;
    public $use;

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}
