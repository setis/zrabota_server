<?php

class daemon {

    protected $fp;
    public $path, $pid, $method = 'run', $pool;

    public function __construct($path) {
        if ($path) {
            $this->path = $path;
        }
        $this->pid = posix_getpid();
    }

    public function open() {
        if (!$this->isDaemonActive()) {
            $this->fp = fopen($this->path, 'w+');
            fwrite($this->fp, $this->pid);
            //не даем записывать
            flock($this->fp, LOCK_EX);
            //выбираем метод запуска
            $this->{$this->method}();
        } else {
            echo 'демон уже запущен pid процесса ' . $this->pid . PHP_EOL;
            exit;
        }
    }

    public function close() {
        flock($this->fp, LOCK_UN);
        fclose($this->fp);
        if (!unlink($this->path)) {
            echo 'не могу удалть pid файл ' . $this->pid . PHP_EOL;
            exit;
        }
    }

    public function isDaemonActive() {
        if (is_file($this->path)) {
            $pid = file_get_contents($this->path);
            //проверяем на наличие процесса
            if (posix_kill($pid, 0)) {
                $this->pid = (int) $pid;
                //демон уже запущен
                return true;
            } else {
                //pid-файл есть, но процесса нет
                if (!unlink($this->path)) {
                    //не могу уничтожить pid-файл. ошибка
                    exit(-1);
                }
            }
        }
        return false;
    }

    public function once() {
        $child_pid = pcntl_fork();
        if ($child_pid) {
            // выходим из родительского, привязанного к консоли, процесса
            exit;
        } else {
            
        }
        posix_setsid();
    }

    abstract public function run() {
        
    }

    public function worker($param) {
        
    }

}

class daemon_multiple extends daemon {
    public $signals = [];
    public function run() {
        pcntl_sigprocmask(SIG_BLOCK, $this->signals);
        $pid = pcntl_fork();
        if ($pid) { 
            $i = 3;
            while (true) {
                echo "sleep $i\n";
                sleep($i);
            }
        } else {
            
        }
    }

}

class pool {

    public $pool = [],
            $signals = [],
            $signals_childer = [],
            $signals_parrent = [],
            $handler_childer = [],
            $handler_parrent = [];
    public $min = 1, $max = 5;

    const lock = 1, unlock = 0;

    public $ppid, $pid;

    public function __construct() {
        $this->ppid = getmypid();
        $this->handler_childer = [
            SIGHUP => [&$this, 'signal_childer_sighup'],
            SIGABRT => [&$this, 'worker'],
        ];
        $this->handler_childer = [
            SIGHUP => [&$this, 'signal_parrent_sighup'],
            SIGQUIT => [&$this, 'signal_parrent_sigquit'],
        ];

        $this->signals_childer = array_keys($this->handler_childer);
        $this->signals_parrent = array_keys($this->handler_parrent);
        $this->signals = array_merge($this->signals_childer, $this->signals_parrent);
    }

    public function run() {
        if (count($this->signals)) {
            pcntl_sigprocmask(SIG_BLOCK, $this->signals);
        }
        while (true) {
            $count = count($this->pool);
            if ($count >= $this->min && $count <= $this->max) {
                $this->pid = pcntl_fork();
            }
            if ($this->pid === -1) {
                //если ошибка при создание процесса    
            } elseif ($this->pid) {
                //главный процесс
                if (count($this->signals_parrent)) {
                    pcntl_sigwaitinfo($this->signals_parrent, $info);
                    if (isset($this->signals_parrent[$info['signo']])) {
                        $method = &$this->signals_parrent[$info['signo']];
                        if (is_array($method)) {
                            call_user_func($method);
                        } elseif (is_callable($method)) {
                            $method();
                        }
                        //посылаем что освободились
                        posix_kill($this->ppid, SIGQUIT);
                    } else {
                        //нет такого воркера
                        unset($this->signals_parrent[$info['signo']]);
                    }
                }
            } else {
                //дочерный процесс

                posix_kill($this->ppid, SIGHUP);
                if (count($this->signals_childer)) {
                    pcntl_sigwaitinfo($this->signals_childer, $info);
                    if (isset($this->signals_childer[$info['signo']])) {
                        $method = &$this->signals_childer[$info['signo']];
                        if (is_array($method)) {
                            call_user_func($method);
                        } elseif (is_callable($method)) {
                            $method();
                        }
                    } else {
                        //нет такого воркера
                        unset($this->signals_childer[$info['signo']]);
                    }
                }
            }
            while ($signaled_pid = pcntl_waitpid(-1, $status, WNOHANG)) {
                if ($signaled_pid == -1) {
                    //детей не осталось
                    $this->pool = array();
                    break;
                } else {
                    unset($this->pool[$signaled_pid]);
                }
            }
        }
    }

    /**
     * @todo Посылаем сигнал что занят дочерный процесс
     */
    public function signal_childer_sighup() {
        echo $this->pid . "\n";
        posix_kill($this->ppid, SIGHUP);
    }

    /**
     * @todo отмечам что процесс занят и посылаем его на выполнение
     */
    public function signal_parrent_sighup() {
        $this->pool[$this->pid] = self::lock;
        echo $this->pid . "\n";
        posix_kill($this->pid, SIGABRT);
    }

    /**
     * @todo отмечам что процесс незанят
     */
    public function signal_parrent_sigquit() {
        echo $this->pid . "\n";
        $this->pool[$this->pid] = self::unlock;
    }

    /**
     * @todo выполняем 
     */
    public function worker() {
        echo $this->pid . "\n";
        file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . $this->pid . 'pid');
    }

}
