<?php defined('SYSPATH') OR die('No direct script access.');

defined('SYSPATH') OR die('No direct script access.');

abstract class Controller extends Kohana_Controller {

    protected $modules = array();

    /*
     * вызов другого контролера через текущий
     */

    public function factory($name) {
        $class = 'Controller_' . $name;
        if (class_exists($class)) {
            return new $class($this->request, $this->response);
        } else {
            if ($path = kohana::find_file(MODPATH, $class)) {
                include $path;
                return new $class($this->request, $this->response);
            }
            throw new ErrorException;
        }
    }

    /*
     * вызов модуля
     */
    public function modul($modul) {
        if (array_key_exists($modul, $this->modules)) {
            return $this->modules[$modul];
        } else {
            $name = str_replace('Controller_', '', get_class($this));
            $conttroler = $name.'_'.ucfirst($modul);
            $obj = $this->factory($conttroler);
            if(is_subclass_of($obj, 'Controller')){
                $this->modules[$modul] = $obj;
                return $this->modules[$modul];
            }else{
                throw new Exception;
            }
            
        }
    }

    public static function run($name){
        $class = 'Controller_' . $name;
        if (class_exists($class)) {
            return new $class();
        } else {
            if ($path = kohana::find_file(MODPATH, $class)) {
                include $path;
                return new $class($this->request, $this->response);
            }
            throw new ErrorException;
        }
    }
}
