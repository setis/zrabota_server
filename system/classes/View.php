<?php

defined('SYSPATH') OR die('No direct script access.');

class View extends Kohana_View {

    public function compile() {
        return (string)$this->render();
    }

}
