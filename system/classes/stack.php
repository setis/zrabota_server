<?php

abstract class stack {

    static protected $stack = array();

}

class stackStatic extends stack {

    static public function &select($select) {
        return self::$stack[$select];
    }

    static public function create($select) {
        self::$stack[$select] = null;
    }

    static public function is($select) {
        return array_key_exists($select, self::$stack);
    }

    static public function bind_to($select, &$var) {
        self::$stack[$select] = $var;
    }

    static public function bind_in($select, &$var) {
        $var = &self::$stack[$select];
    }

}

class stackThis extends stack {

    private $select;

    public function __construct($select, &$bind_in) {
        $this->select = $select;
        $this->bind_in($bind_in);
    }

    public function _use($select) {
        $this->select = $select;

        return $this;
    }

    public function &select() {
        return self::$stack[$this->select];
    }

    public function create($data) {
        self::$stack[$this->select] = $data;
        return $this;
    }

    public function delete() {
        unset(self::$stack[$this->select]);
        return $this;
    }

    public function is() {
        return array_key_exists($this->select, self::$stack);
    }

    public function bind_to(&$var) {
        self::$stack[$this->select] = $var;
        return $this;
    }

    public function bind_in(&$var) {
        $var = &self::$stack[$this->select];
        return $this;
    }

}

?>
