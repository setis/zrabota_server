<?php

class modules {

    protected $stack;
    protected $modules = array();
    protected $registation = 'class';

    public function __construct() {
        $this->stack = new stackThis(__CLASS__);
        if (!$this->stack->is()) {
            $this->stack->bind_to($this->modules);
        } else {
            $this->stack->bind_in($this->modules);
        }
    }

    public function load($modul, $name = null) {
        return $this->modules[$name] = $modul;
    }

    public function unload($name) {
        unset($this->modules[$name]);
    }

    public function loaded($module) {
        if (is_string($module)) {
            return array_key_exists($module, $this->modules);
        }
    }

    public function registation($module) {
        return $this->{'registation_' . $this->registation}($module);
    }

    public function setRegistation($type) {
        if (method_exists(&$this, 'registation_' . $type)) {
            $this->registation = $type;
            return true;
        }
        return false;
    }

    public function getRegistation() {
        return $this->registation;
    }

    public function registation_class($module) {
        try {
            $this->modules[get_class($module)] = $module;
        } catch (ErrorException $e) {
            return false;
        }
        return true;
    }

    public function registation_push($module) {
        return (bool) array_push($this->modules, $module);
    }

}

class modul {
    //put your code here
}

?>
